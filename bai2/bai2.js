/**
 * input: 5 số thực
 * 
 * progress:
 * + khởi tạo function calAge
 *   trong hàm calAge
 * + lấy lần lượt id thẻ input thông qua DOM và lấy value * 1 gán lại các biến first, second, third, fourth, fifth
 * + khởi tạo biến avg = (Value1 + value2 + value3 + value4 + value5) / 5
 * + lấy id thẻ h3 thông qua DOM
 * in giá trị avg thông qua h3 với phương thức innerHTML
 * 
 * output: trung binh cộng 5 số thực
 */


function calAvg() {
    var first = document.getElementById("first").value * 1;
    var second = document.getElementById("second").value * 1;
    var third = document.getElementById("third").value * 1;
    var fourth = document.getElementById("fourth").value * 1;
    var fifth = document.getElementById("fifth").value * 1;
    var avg = (first + second + third + fourth + fifth) / 5;
    document.getElementById("result").textContent = "Result: " + avg;
}