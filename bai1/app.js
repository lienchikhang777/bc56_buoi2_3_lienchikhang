/**
 * input: số ngày làm việc
 * 
 * progress:
 * lấy id thẻ input thông qua DOM và gán vào biến input
 * lấy id thẻ h3 thông qua DOM và gán vào biến h3
 * khởi tạo biến salaryPerDay = 100000
 * tạo function calSalary để tính lương
 * khởi tạo biến workingDay = giá trị value của input
 * khởi tạo biến salary = workingDay * salaryPerDay
 * hiển thị giá trị thông qua thẻ h3 bằng phương thức textContent
 * 
 * output: tổng lương
 */




var input = document.getElementById("workingDay");
var h3 = document.getElementById("result");
const SALARY_PER_DAY = 100000;

function calSalary() {
    var workingDay = input.value * 1;
    var salary = SALARY_PER_DAY * workingDay;
    h3.textContent = 'Lương của bạn: ' + salary.toLocaleString();
}