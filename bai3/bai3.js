/**
 * input: số tiền usd
 * 
 * progress: 
 * + lấy id thẻ input thông qua DOM và gán vào biến input
 * + lấy id thẻ h3 thông qua DOM và gán vào biến h3
 * + khởi tạo biến basePrice = 23500
 * + tạo hàm transToVND()
 * + trong hàm transToVND()
 *      + khởi tạo biến usdPrice và gán bằng value của biến input
 *      + khởi tạo biến result = usdPrice * basePrice
 *      + in giá trị result thông qua biến h3 với phương thức textContent
 * output: số tiền vnd
 */

var h3 = document.getElementById("result");
const BASE_PRICE = 23500;
function transToVND() {
    var usdPrice = document.getElementById("usdMoney").value * 1;
    var result = usdPrice * BASE_PRICE;
    h3.textContent = "Số tiền sau khi chuyển: " + result.toLocaleString() + " VND";
}