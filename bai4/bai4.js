/**
 * input: chiều dài, chiều rộng
 * 
 * progress: 
 * + khởi tạo hàm calculate()
 *      + lấy id thẻ input thứ 1 thông qua DOM,lấy value * 1 và gán vào biến lengthValue
 *      + lấy id thẻ input thứ 2 thông qua DOM,lấy value * 1 và gán vào biến widthValue
 *      + khởi taọ biến perimeter = (lengthValue + widthValue) * 2
 *      + khởi tạo biến area = lengthValue * widthValue
 *      + lấy id thẻ h3 thông qua DOM
 *      + in 2 gía trị perimeter và area thông qua h3 với phương thức innerHTML
 * 
 * output: chu vi, diện tích hcn
 */


function calculate() {
    var lengthValue = document.getElementById("length").value * 1;
    var widthValue = document.getElementById("width").value * 1;
    var perimeter = (lengthValue + widthValue) * 2
    var area = lengthValue * widthValue;
    document.getElementById("result").innerHTML = `
        <p>Chu vi: ${perimeter}</p>
        <p>Diện tích: ${area}</p>
    `
}