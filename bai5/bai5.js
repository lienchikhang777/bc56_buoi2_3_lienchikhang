/**
 * input: 1 số có 2 kí số
 * 
 * progress: 
 *  tạo hàm sumOfTwoChar()
 * trong hàm sumOfTwoChar:
 *      + lấy id thẻ input thông qua DOM, lấy value * 1 và gán vào biến number
 *      + tạo biến ten = Math.floor(number / 10);
 *      + tạo biến unit = number % 10;
 *      + tạo biến sum = ten + unit
 *      + lấy id thẻ div thông qua DOM
 *      + in biến sum thông qua div#result với phương thức innerHTML
 * 
 * output: tổng 2 kí số
 */

function sumOfTwoChar() {
    var number = document.getElementById("number").value * 1;
    var ten = Math.floor(number / 10);
    var unit = number % 10;
    var sum = ten + unit;
    document.getElementById("result").innerHTML = `
        Result: ${ten} + ${unit} = ${sum}
    `
}
